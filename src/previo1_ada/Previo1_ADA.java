/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package previo1_ada;

/**
 *
 * @author coloque acá su nombres y código
 */
public class Previo1_ADA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int vector1[]={2,4,5,1,3,2};
        int vector2[]={2,4,5,1,3};
        System.out.println("Número prohibido:"+getNumeroProhibido(vector1));
        System.out.println("Número prohibido:"+getNumeroProhibido(vector2));
    }
    
    
    public static int getNumeroProhibido(int vector[])
    {
        int n=0;
        int con=0;
        for(int i=0; i<vector.length; i++){
            n=vector[i];
            for(int j=0; j<vector.length; j++){
                if(n==vector[j]) con++;
                if(con>1) return n;
            }
            con=0;
        }
        return -1;
    }
}
